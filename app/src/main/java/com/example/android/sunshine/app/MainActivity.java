package com.example.android.sunshine.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android.sunshine.app.sync.SunshineSyncAdapter;

/*
Sunshine-Version-2: Allows the user to click on an item in a ListView in MainActivity which is filled
with weather data for a certain place for 13 days. After an item is clicked a detailpage is being
shown with an extra option to Share and another option to change the Settings in the menu. In
MainActivity menu also has the option to change Settings as well as the option "See location on map".
Thus the app has 2 Activities (And a SettingsActivity, so 3 actually). MainActivity uses default
location, or otherwise location saved in Preference object (in SettingsActivity onPreferenceChange())
to retrieve weather data of today and the coming 12 days. SunshineSyncAdapter is started from the
MainActivity (in onCreate() with a call to instance method initializeSyncAdapter()) which causes data
to be retrieved from Open Weather Map(By synchronizing local and remote data). SunshineSyncAdapter
lives inside a bounded Service which is started when onBind() is being called. When data is retrieved
it is being saved in a local SQLite Database (With a call to bulkInsert() on ContentResolver in
method getWeatherDataFromJson() which in turn is called in method onPerformSync() (the most important
method in class SunshineSyncAdapter where the network code is being placed)). Then data can then be
accessed through ContentProvider via a ContentResolver (Used to query and perform Transactions on
those Content Providers). MainActivity starts DetailActivity when in portrait mode (and user clicks
on an item in the ListView) and DetailFragment in landscape mode (two-pane mode (res/layout-xlarge)).
In MainActivity ForecastFragment is shown. ForecastFragment shows a ListView which is being filled
by the ForecastAdapter (Custom CursorAdapter). However the data is first being retrieved through a
CursorLoader (ForecastFragment implements LoaderManager.LoaderCallbacks<Cursor> and has to override
3 methods..) which returns a Cursor (Result set returned by a database query). In onLoadFinished()
the ForecastAdapter object swaps the data from the Cursor, so that ListView shows the new data.
When DetailActivity is shown in portrait mode it uses the getSupportFragmentManager() to add
DetailFragment to it (which has it's own xml (fragment_detail.xml)). DetailFragment also implements
LoaderManager.LoaderCallbacks<Cursor> which uses a URI to retrieve the correct set of data in a
through a CursorLoader which in turn returns a Cursor(Result set returned by a database query).
In onLoadFinished() the right labels are being filled with text and an image. Lastly there's a
SettingsActivity which extends from PreferenceActivity. It ensures that a preference can be set.
It also implements Preference.OnPreferenceChangeListener which enables the user to change preferences
via the onPreferenceChange() which must be overridden.
 */

public class MainActivity extends ActionBarActivity implements ForecastFragment.Callback {

    //FIELDS
    private static final String DETAILFRAGMENT_TAG = "DFTAG";
    private final String LOG_TAG = MainActivity.class.getSimpleName();
    public boolean mTwoPane;
    private String mLocation;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocation = Utility.getPreferredLocation(this);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.weather_detail_container) != null) {
            // The detail container view will be present only in the large-screen layouts
            // (res/layout-xlarge). If this view is present, then the activity should be
            // in two-pane mode.
            mTwoPane = true;
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            //Add a Fragment Tag
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.weather_detail_container, new DetailFragment(), DETAILFRAGMENT_TAG)
                        .commit();
            } else {
                mTwoPane = false;
                getSupportActionBar().setElevation(0f);
            }
        }

        ForecastFragment forecastFragment = ((ForecastFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_forecast));
        forecastFragment.setUseTodayLayout(!mTwoPane);

        Log.v(LOG_TAG, "THIS IS: onCreate()");

        SunshineSyncAdapter.initializeSyncAdapter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v(LOG_TAG, "THIS IS: onStart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(LOG_TAG, "THIS IS: onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(LOG_TAG, "THIS IS: onResume()");

        //Check whether location has changed compared to what is stored in the settings
        String location = Utility.getPreferredLocation(this);

        // update the location in our second pane using the fragment manager
        if (location != null && !location.equals(mLocation)) {
            ForecastFragment ff = (ForecastFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
            if (null != ff) {
                ff.onLocationChanged();
            }
            DetailFragment df = (DetailFragment) getSupportFragmentManager().findFragmentByTag(DETAILFRAGMENT_TAG);
            if (null != df) {
                df.onLocationChanged(location);
            }

            //Update Location
            mLocation = location;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v(LOG_TAG, "THIS IS: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "THIS IS: onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * DetailFragmentCallback for when an item has been selected.
     *
     * @param dateUri
     */
    @Override
    public void onItemSelected(Uri contentUri) {

        //When in two-pane mode on Tablet replace DetailFragment
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle args = new Bundle();
            args.putParcelable(DetailFragment.DETAIL_URI, contentUri);

            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(args);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.weather_detail_container, fragment, DETAILFRAGMENT_TAG)
                    .commit();

        } else //When on Phone
        {
            Intent intent = new Intent(this, DetailActivity.class).setData(contentUri);
            startActivity(intent);
        }
    }


}
